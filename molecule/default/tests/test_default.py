import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_default(host):
    check = host.run("sudo kubectl get pods --all-namespaces")
    assert check.succeeded
    assert "kube-system" in check.stdout

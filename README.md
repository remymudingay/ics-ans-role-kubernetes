# ics-ans-role-kubernetes

Ansible role to install kubernetes.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-kubernetes
```

## License

BSD 2-clause

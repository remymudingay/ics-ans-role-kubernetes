---
- name: Include OS-specific variables.
  include_vars: "{{ ansible_os_family }}.yml"

- include_tasks: "{{ ansible_os_family }}.yml"

- name: Ensure dependencies are installed.
  package:
    name: curl
    state: present

- name: Install Kubernetes packages.
  package:
    name: "{{ item.name | default(item) }}"
    state: "{{ item.state | default('present') }}"
  notify: restart kubelet
  with_items: "{{ kubernetes_packages }}"
  register: kubernetes_installed

- name: Disable SWAP since kubernetes can't work with swap enabled (1/2)
  shell: |
    swapoff -a
  when: kubernetes_installed is changed

- name: Disable SWAP in fstab since kubernetes can't work with swap enabled (2/2)
  replace:
    path: /etc/fstab
    regexp: '^/swapfile'
    replace: '#/swapfile'
  when: kubernetes_installed is changed

- include_tasks: sysctl-setup.yml

- name: Ensure kubelet is started and enabled at boot.
  systemd:
    name: kubelet
    state: started
    enabled: true

- name: Check if Kubernetes has already been initialized.
  stat:
    path: /etc/kubernetes/admin.conf
  register: kubernetes_init_stat

# Set up master.
- include_tasks: master-setup.yml
  when: kubernetes_role == 'master'

# Set up nodes.
- name: Get the kubeadm join command from the Kubernetes master.
  command: kubeadm token create --print-join-command
  changed_when: false
  when: kubernetes_role == 'master'
  register: kubernetes_join_command_result

- name: Set the kubeadm join command globally.
  set_fact:
    kubernetes_join_command: >
      {{ kubernetes_join_command_result.stdout }}
      {{ kubernetes_join_command_extra_opts }}
  when: kubernetes_join_command_result.stdout is defined
  delegate_to: "{{ item }}"
  delegate_facts: true
  with_items: "{{ groups['all'] }}"

- include_tasks: node-setup.yml
  when: kubernetes_role == 'node'
